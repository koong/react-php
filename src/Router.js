import React from "react"
import {Switch} from "react-router-dom"
import {PublicRoute} from "./components/layouts"

import Home from "./pages/home"

class AppRouter extends React.Component {
    render() {
        const publicRouting = (
            <Switch>
                <PublicRoute path="/" component={Home} />
            </Switch>
        )
        return publicRouting
    }
}

export default AppRouter
