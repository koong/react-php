export const imgSrc = (image, upload = false) => {
    return upload ? "/img/upload/" + image : "/img/" + image
}

export const apiUrl = (uri = "") => {
    const {location} = window
    const apiLink = location.hostname === "localhost" ? "http://localhost:9000" : "http://" + location.hostname + "/server"
    return apiLink + "/" + uri
}
