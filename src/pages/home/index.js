import React from "react"

class Home extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <h1>H1</h1>
                    <h2>H2</h2>
                    <h3>H3</h3>
                    <h4>H4</h4>
                    <h5>H5</h5>
                    <h6>H6</h6>
                    <p>Paragraph: Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores fugiat blanditiis vero, omnis facilis debitis quo officia. Quam recusandae, velit vel fugiat, nihil, repudiandae veniam vitae eum assumenda omnis perspiciatis.</p>
                    <ul>
                        <li>Container width: <b>1200px</b></li>
                        <li>Font: <b>opensans</b></li>
                        <li>Connect: <b>MySQL Server</b></li>
                        <li>Server: <b>PHP</b></li>
                    </ul>
                </div>
            </React.Fragment>
        )
    }
}

export default Home
