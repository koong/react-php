import React from "react"

class HeaderComponent extends React.Component {
    render() {
        return (
            <header>
                <div style={{background: "blue", color: "white",padding: 15}}>Header</div>
            </header>
        )
    }
}

export default HeaderComponent
