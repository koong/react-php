import React from "react"
import {Route} from "react-router-dom"
import Header from "./header"
import Footer from "./footer"

export const PublicRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={(props) => {
            return (
                <React.Fragment>
                    <Header />
                    <main>
                        <Component {...props} {...rest} />
                    </main>
                    <Footer />
                </React.Fragment>
            )
        }}
    />
)
