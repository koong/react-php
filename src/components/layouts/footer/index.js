import React from "react"

class FooterComponent extends React.Component {
    render() {
        return (
            <footer>
                <div style={{background: "#393939", color: "#fff", padding: 15}}>Footer</div>
            </footer>
        )
    }
}

export default FooterComponent
