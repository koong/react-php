import React from "react"
import ReactDOM from "react-dom"
import {Router} from "react-router-dom"
import createHistory from "history/createBrowserHistory"

import AppRouter from "./Router"

import "antd/dist/antd.min.css"
import "./assets/css/index.css"

const render = (Component) => {
    return ReactDOM.render(
        <Router history={createHistory()}>
            <Component />
        </Router>,
        document.getElementById("root")
    )
}

render(AppRouter)
